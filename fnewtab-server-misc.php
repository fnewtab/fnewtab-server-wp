<?php
namespace FNewTabWP;

/**
 * Miscellaneous
 */
class Misc
{
    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'plugins_loaded', [ $this, 'load_textdomain' ] );
    }

    /**
     * Load text domain
     */
    public function load_textdomain()
    {
        load_plugin_textdomain( 'fnewtab-server', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
    }
}