jQuery( function() {
    jQuery('#linksAdd').show();
    jQuery('#linksContainer').show();

    var removeLinkCallback = function() {
        var i = parseInt( jQuery( this ).attr( 'data-i' ) );
        jQuery( '#links' + i ).remove();
        jQuery( '#linksHr' + i ).remove();
    };
    jQuery( '.links-remove' ).on( 'click', removeLinkCallback );

    var imagePickerCallback = function() {
        var i = parseInt( jQuery( this ).attr( 'data-i' ) );

        links_picking = true;

        original_send_attachment = wp.media.editor.send.attachment;
        wp.media.editor.send.attachment = function(props, attachment) {
            if ( links_picking ) {
                jQuery( '#linksImage' + i ).val( attachment.url );
                links_picking = false;
            } else {
                return original_send_attachment.apply( this, [props, attachment] );
            };
        }
        wp.media.editor.open(jQuery( this ));
    }
    jQuery( '.links-image-picker' ).on( 'click', imagePickerCallback );

    jQuery( '#linksAdd' ).on( 'click', function() {
        var i = parseInt( jQuery( '#linksI' ).val() );

        if ( jQuery( '#linksContainer' ).children().length != 0 ) {
            var hr = document.createElement( 'hr' );
            jQuery( hr ).attr( 'id', 'linksHr' + i );
            jQuery( '#linksContainer' ).append( hr );
        }

        var table = document.createElement( 'table' );
        jQuery( table ).attr( 'id', 'links' + i );

        var nameLabel = document.createElement( 'th' );
        jQuery( nameLabel ).attr( 'scope', 'row' );
        jQuery( nameLabel ).text( fnewtabTranslations['Name'] );
        var nameInput = document.createElement( 'input' );
        jQuery( nameInput ).attr( 'type', 'text' );
        jQuery( nameInput ).attr( 'id', 'linksName' + i );
        jQuery( nameInput ).attr( 'name', 'fnewtab_server_options_links[' + i + '][name]' );
        var nameTd = document.createElement( 'td' );
        jQuery( nameTd ).append( nameInput );
        var nameTr = document.createElement( 'tr' );
        jQuery( nameTr ).append( nameLabel );
        jQuery( nameTr ).append( nameTd );
        jQuery( table ).append( nameTr );

        var urlLabel = document.createElement( 'th' );
        jQuery( urlLabel ).attr( 'scope', 'row' );
        jQuery( urlLabel ).text( fnewtabTranslations['URL'] );
        var urlInput = document.createElement( 'input' );
        jQuery( urlInput ).attr( 'type', 'url' );
        jQuery( urlInput ).attr( 'id', 'linksUrl' + i );
        jQuery( urlInput ).attr( 'name', 'fnewtab_server_options_links[' + i + '][url]' );
        var urlTd = document.createElement( 'td' );
        jQuery( urlTd ).append( urlInput );
        var urlTr = document.createElement( 'tr' );
        jQuery( urlTr ).append( urlLabel );
        jQuery( urlTr ).append( urlTd );
        jQuery( table ).append( urlTr );

        var imageLabel = document.createElement( 'th' );
        jQuery( imageLabel ).attr( 'scope', 'row' );
        jQuery( imageLabel ).text( fnewtabTranslations['Image'] );
        var imageInput = document.createElement( 'input' );
        jQuery( imageInput ).attr( 'type', 'text' );
        jQuery( imageInput ).attr( 'id', 'linksImage' + i );
        jQuery( imageInput ).attr( 'name', 'fnewtab_server_options_links[' + i + '][image]' );
        var imagePicker = document.createElement( 'input' );
        jQuery( imagePicker ).attr( 'type', 'button' );
        jQuery( imagePicker ).attr( 'id', 'linksImagePick' + i );
        jQuery( imagePicker ).attr( 'data-i', i );
        jQuery( imagePicker ).attr( 'value', fnewtabTranslations['Media library'] );
        jQuery( imagePicker ).addClass( 'button' );
        jQuery( imagePicker ).on( 'click', imagePickerCallback );
        var imageTd = document.createElement( 'td' );
        jQuery( imageTd ).append( imageInput );
        jQuery( imageTd ).append( imagePicker );
        var imageTr = document.createElement( 'tr' );
        jQuery( imageTr ).append( imageLabel );
        jQuery( imageTr ).append( imageTd );
        jQuery( table ).append( imageTr );

        var removeLabel = document.createElement( 'th' );
        jQuery( removeLabel ).attr( 'scope', 'row' );
        var removeInput = document.createElement( 'input' );
        jQuery( removeInput ).attr( 'type', 'button' );
        jQuery( removeInput ).attr( 'id', 'linksRemove' + i );
        jQuery( removeInput ).attr( 'data-i', i );
        jQuery( removeInput ).attr( 'value', fnewtabTranslations['Remove'] );
        jQuery( removeInput ).addClass( 'button' );
        jQuery( removeInput ).on( 'click', removeLinkCallback );
        var removeTd = document.createElement( 'td' );
        jQuery( removeTd ).append( removeInput );
        var removeTr = document.createElement( 'tr' );
        jQuery( removeTr ).append( removeLabel );
        jQuery( removeTr ).append( removeTd );
        jQuery( table ).append( removeTr );

        jQuery( '#linksContainer' ).append( table );

        jQuery( '#linksI' ).val(i + 1);
    } );
} );