<?php
namespace FNewTabWP;

class Options
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;
    private $option_links;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', [ $this, 'add_plugin_page' ] );
        add_action( 'admin_init', [ $this, 'page_init' ] );
        add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_scripts' ] );
    }

    /**
     * Enqueue the link editor script if the current page is FNewTab options
     */
    public function enqueue_scripts( $hook )
    {
        if ( $hook != 'settings_page_fnewtab-server-options' ) return;

        wp_enqueue_script(
            'fnewtab-server-options-links',
            plugin_dir_url( __FILE__ ) . 'fnewtab-server-options-links.js',
            [ 'jquery', 'media-upload' ]
        );

        wp_localize_script( 'fnewtab-server-options-links', 'fnewtabTranslations', [
            'Name' => __( 'Name', 'fnewtab-server' ),
            'URL' => __( 'URL', 'fnewtab-server' ),
            'Image' => __( 'Image', 'fnewtab-server' ),
            'Media library' => __( 'Media library', 'fnewtab-server' ),
            'Remove' => __( 'Remove', 'fnewtab-server' )
        ] );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            __( 'FNewTab Settings', 'fnewtab-server' ),
            __( 'FNewTab', 'fnewtab-server' ),
            'manage_options',
            'fnewtab-server-options',
            [ $this, 'create_admin_page' ]
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'fnewtab_server_options' );
        $this->option_links = get_option( 'fnewtab_server_options_links', [] );
        ?>
        <div class="wrap">
            <h1><?php _e( 'FNewTab Settings', 'fnewtab-server' ); ?></h1>
            <p><?php printf( __( 'FNewTab index URL: %s', 'fnewtab-server' ), '<code>' . rest_url('/fnewtab/v1/index') . '</code>' ); ?></code></p>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'fnewtab_server_options' );
                do_settings_sections( 'fnewtab-server-options' );
                submit_button();
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'fnewtab_server_options', // Option group
            'fnewtab_server_options', // Option name
            [ $this, 'sanitize' ] // Sanitize
        );

        register_setting(
            'fnewtab_server_options', // Option group
            'fnewtab_server_options_links', // Option name
            [ $this, 'sanitize_links' ] // Sanitize
        );

        add_settings_section(
            'fnewtab-server-options-basic', // ID
            __( 'Basic options', 'fnewtab-server' ), // Title
            [ $this, 'print_basic_section_info' ], // Callback
            'fnewtab-server-options' // Page
        );

        add_settings_field(
            'show_search', // ID
            __( 'Show search bar', 'fnewtab-server' ), // Title 
            [ $this, 'show_search_callback' ], // Callback
            'fnewtab-server-options', // Page
            'fnewtab-server-options-basic' // Section           
        );

        add_settings_field(
            'enable_links', // ID
            __( 'Enable links', 'fnewtab-server' ), // Title 
            [ $this, 'enable_links_callback' ], // Callback
            'fnewtab-server-options', // Page
            'fnewtab-server-options-basic' // Section           
        );

        add_settings_field(
            'enable_message', // ID
            __( 'Enable message', 'fnewtab-server' ), // Title 
            [ $this, 'enable_message_callback' ], // Callback
            'fnewtab-server-options', // Page
            'fnewtab-server-options-basic' // Section           
        );

        add_settings_section(
            'fnewtab-server-options-links', // ID
            __( 'Links', 'fnewtab-server' ), // Title
            [ $this, 'print_links_section_info' ], // Callback
            'fnewtab-server-options' // Page
        );

        add_settings_field(
            'links', // ID
            __( 'Links', 'fnewtab-server' ), // Title 
            [ $this, 'links_callback' ], // Callback
            'fnewtab-server-options', // Page
            'fnewtab-server-options-links' // Section           
        );

        add_settings_section(
            'fnewtab-server-options-message', // ID
            __( 'Message', 'fnewtab-server' ), // Title
            [ $this, 'print_message_section_info' ], // Callback
            'fnewtab-server-options' // Page
        );

        add_settings_field(
            'message', // ID
            __( 'Message', 'fnewtab-server' ), // Title 
            [ $this, 'message_callback' ], // Callback
            'fnewtab-server-options', // Page
            'fnewtab-server-options-message' // Section           
        );
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = [];
        if( isset( $input['show_search'] ) )
            $new_input['show_search'] = filter_var( $input['show_search'], FILTER_VALIDATE_BOOLEAN );

        if( isset( $input['enable_links'] ) )
            $new_input['enable_links'] = filter_var( $input['enable_links'], FILTER_VALIDATE_BOOLEAN );
        
        if( isset( $input['enable_message'] ) )
            $new_input['enable_message'] = filter_var( $input['enable_message'], FILTER_VALIDATE_BOOLEAN );

        if( isset( $input['message'] ) )
            $new_input['message'] = $input['message'];

        return $new_input;
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize_links( $input )
    {
        $new_input = [];
        foreach ( $input as $link ) {
            if (
                is_array( $link )
                && isset( $link['name'] )
                && isset( $link['url'] )
                && isset( $link['image'] )
            ) $new_input[] = $link;
        }

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_basic_section_info()
    {
        _e( 'Select which features to enable on the FNewTab homepage.', 'fnewtab-server' );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function show_search_callback()
    {
        printf(
            '<input type="checkbox" id="show_search" name="fnewtab_server_options[show_search]" %s />',
            isset( $this->options['show_search'] ) ? checked( $this->options['show_search'], true, false ) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function enable_links_callback()
    {
        printf(
            '<input type="checkbox" id="enable_links" name="fnewtab_server_options[enable_links]" %s />',
            isset( $this->options['enable_links'] ) ? checked( $this->options['enable_links'], true, false ) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function enable_message_callback()
    {
        printf(
            '<input type="checkbox" id="enable_message" name="fnewtab_server_options[enable_message]" %s />',
            isset( $this->options['enable_message'] ) ? checked( $this->options['enable_message'], true, false ) : ''
        );
    }

    /** 
     * Print the Section text
     */
    public function print_links_section_info()
    {
        _e( 'Define the links to be shown on the FNewTab page.', 'fnewtab-server' );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function links_callback()
    {
        $links = [];

        if ( is_array( $this->option_links ) ) {
            $links = $this->option_links;
        }

        ?>
            <noscript>
                <p><?php _e( 'JavaScript is required to edit links.', 'fnewtab-server' ); ?></p>
            </noscript>
            <div id="linksContainer" style="display:none">
                <?php
                    // note: linksContainer has display:none because it is shown by the script so that it is not visible if JavaScript is disabled.
                    $i = 0;
                    foreach ( $links as $link ) {
                        if ($i != 0) printf(
                            '<hr id="linksHr%s" />',
                            (string)$i
                        );

                        ?> <table id="links<?php
                            print (string)$i;
                        ?>"><tr><th scope="row"> <?php
                            _e( 'Name', 'fnewtab-server' );
                        ?> </th><td> <?php
                        printf(
                            '<input type="text" id="linksName%s" name="fnewtab_server_options_links[%s][name]" value="%s" />',
                            (string)$i, (string)$i,
                            $link['name']
                        );
                        ?> </td></tr><tr><th scope="row"> <?php
                            _e( 'URL', 'fnewtab-server' );
                        ?> </th><td> <?php
                        printf(
                            '<input type="url" id="linksUrl%s" name="fnewtab_server_options_links[%s][url]" value="%s" />',
                            (string)$i, (string)$i,
                            $link['url']
                        );
                        ?> </td></tr><tr><th scope="row"> <?php
                            _e( 'Image', 'fnewtab-server' );
                        ?> </th><td> <?php
                        printf(
                            '<input type="text" id="linksImage%s" name="fnewtab_server_options_links[%s][image]" value="%s" />',
                            (string)$i, (string)$i,
                            $link['image']
                        );
                        printf(
                            '<input type="button" id="linksImagePick%s" class="links-image-picker button" data-i="%s" value="%s" />',
                            (string)$i, (string)$i,
                            __( 'Media library', 'fnewtab-server' )
                        );
                        ?> </td></tr><tr><th scope="row"></th><td> <?php
                        printf(
                            '<input type="button" id="linksRemove%s" class="links-remove button" data-i="%s" value="%s" />',
                            (string)$i, (string)$i,
                            __( 'Remove', 'fnewtab-server' )
                        );
                        ?> </td></tr></table> <?php

                        $i++;
                    }
                ?>
            </div>
            <input type="button" class="button" id="linksAdd" value="Add" style="display:none">
        <?php

        printf(
            '<input type="hidden" id="linksI" value="%s" />',
            (string)$i // to provide the value of i to the client-side javascript
        );

    }

    /** 
     * Print the Section text
     */
    public function print_message_section_info()
    {
        _e( 'Define the message to be shown on the FNewTab page. Shortcodes are supported.', 'fnewtab-server' );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function message_callback()
    {
        $message = '';
        wp_editor(
            isset( $this->options['message'] ) ? $this->options['message'] : '',
            'message',
            [ 'textarea_name' => 'fnewtab_server_options[message]' ]
        );
    }
}