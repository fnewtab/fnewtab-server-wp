<?php
namespace FNewTabWP;

class REST
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;
    private $option_links;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'rest_api_init', [ $this, 'register_routes' ] );
    }

    /**
     * Create CORS headers
     */
    private function fix_cors()
    {
        remove_filter( 'rest_pre_serve_request', 'rest_send_cors_headers' );

        add_filter( 'rest_pre_serve_request', function( $value ) {
            header( 'Access-Control-Allow-Origin: *' );
            $matches = [];
            if ( preg_match(
                '/^moz-extension:\/\/[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}/',
                get_http_origin(),
                $matches
            ) ) {
                header( 'Access-Control-Allow-Origin: ' . $matches[0] );
            }
        } );
    }

    /**
     * Registers the REST endpoints (index and message)
     */
    public function register_routes()
    {
        $this->options = get_option( 'fnewtab_server_options' );
        $this->option_links = get_option( 'fnewtab_server_options_links', [] );

        register_rest_route( 'fnewtab/v1', '/index', [
            'methods' => 'GET',
            'callback' => [ $this, 'index' ],
        ] );

        register_rest_route( 'fnewtab/v1', '/message', [
            'methods' => 'GET',
            'callback' => [ $this, 'message' ],
        ] );
    }

    /**
     * Index file
     */
    public function index()
    {
        $this->fix_cors();

        $data = [
            'version' => 1
        ];

        $data['search'] = isset( $this->options['show_search'] ) && $this->options['show_search'];

        if ( isset( $this->options['enable_links'] ) && $this->options['enable_links'] && is_array( $this->option_links ) )
            $data['links'] = $this->option_links;
        
        if ( isset( $this->options['enable_message'] ) && $this->options['enable_message'] && isset( $this->options['message'] ) )
            $data['message'] = rest_url( '/fnewtab/v1/message' );
        
        return $data;
    }

    /**
     * Message endpoint
     */
    public function message()
    {
        $this->fix_cors();

        if ( isset( $this->options['enable_message'] ) && $this->options['enable_message'] )
            return [
                'html' => isset( $this->options['message'] ) ? do_shortcode( $this->options['message'] ) : ''
            ];
        else
            return [
                'html' => ''
            ];
    }
}