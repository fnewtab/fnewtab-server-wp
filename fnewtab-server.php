<?php
/*
Plugin Name:  FNewTab Server for WordPress
Plugin URI:   https://gitlab.com/fnewtab/fnewtab-server-wp#readme
Description:  A WordPress plugin to configure FNewTab's managed homepage.
Version:      1
Author:       Marco Benzoni
Author URI:   https://qlcvea.com
License:      MIT
Domain Path:  /languages
*/

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'fnewtab-server-misc.php';
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'fnewtab-server-options.php';
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'fnewtab-server-rest.php';

$fnewtab_server_misc = new FNewTabWP\Misc();

if( is_admin() )
    $fnewtab_server_options = new FNewTabWP\Options();

$fnewtab_server_rest = new FNewTabWP\REST();